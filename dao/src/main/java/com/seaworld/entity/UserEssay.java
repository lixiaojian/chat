package com.seaworld.entity;

import java.util.Date;

public class UserEssay {
    private Integer id;

    private Integer userId;

    private String image;

    private String title;

    private String content;

    private Integer upCount;

    private Date createTime;

    public UserEssay(Integer id, Integer userId, String image, String title, String content, Integer upCount, Date createTime) {
        this.id = id;
        this.userId = userId;
        this.image = image;
        this.title = title;
        this.content = content;
        this.upCount = upCount;
        this.createTime = createTime;
    }

    public UserEssay() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getUpCount() {
        return upCount;
    }

    public void setUpCount(Integer upCount) {
        this.upCount = upCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
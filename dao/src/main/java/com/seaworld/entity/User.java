package com.seaworld.entity;

import java.util.Date;

public class User {
    private Integer id;

    private String headImage;

    private String name;

    private String nickName;

    private String job;

    private String skills;

    private String hobbies;

    private Date birthday;

    private String brief;

    private Integer age;

    private Integer type;

    private Date createTime;

    private Integer status;

    public User(Integer id, String headImage, String name, String nickName, String job, String skills, String hobbies, Date birthday, String brief, Integer age, Integer type, Date createTime, Integer status) {
        this.id = id;
        this.headImage = headImage;
        this.name = name;
        this.nickName = nickName;
        this.job = job;
        this.skills = skills;
        this.hobbies = hobbies;
        this.birthday = birthday;
        this.brief = brief;
        this.age = age;
        this.type = type;
        this.createTime = createTime;
        this.status = status;
    }

    public User() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage == null ? null : headImage.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job == null ? null : job.trim();
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills == null ? null : skills.trim();
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies == null ? null : hobbies.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief == null ? null : brief.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
package com.seaworld.mapper;

import com.seaworld.entity.UserEssay;
import com.seaworld.entity.UserEssayExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface UserEssayMapper {
    int countByExample(UserEssayExample example);

    int deleteByExample(UserEssayExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserEssay record);

    int insertSelective(UserEssay record);

    List<UserEssay> selectByExample(UserEssayExample example);

    UserEssay selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserEssay record, @Param("example") UserEssayExample example);

    int updateByExample(@Param("record") UserEssay record, @Param("example") UserEssayExample example);

    int updateByPrimaryKeySelective(UserEssay record);

    int updateByPrimaryKey(UserEssay record);
}
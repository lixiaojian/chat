package com.seaworld.server;

import com.github.pagehelper.PageInfo;
import com.seaworld.responseDTO.UserDTO;
import com.seaworld.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 * Created by lixiaojian on 2017/11/10.
 */
@EnableEurekaClient
@RestController("userInfo")
@CrossOrigin(origins = "*")
public class ApiService {

    @Autowired
    UserService userService;
    @RequestMapping("user/get")
    public UserDTO getUser(@RequestParam String name){
        UserDTO userDTO = new UserDTO();
        PageInfo pageInfo = userService.getUserByPage();
        System.out.println("result:"+pageInfo.getPageNum());
        userDTO.setAge(18);
        userDTO.setName("李小健");
        return userDTO;
    }
}

package com.seaworld.server;

import com.github.pagehelper.PageInfo;
import com.seaworld.BO.UserEssayQuery;
import com.seaworld.entity.UserEssay;
import com.seaworld.responseDTO.UserEssayDTO;
import com.seaworld.service.UserEssayService;
import com.seaworld.utils.PageResult;
import com.seaworld.utils.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Description:
 * Created by lixiaojian on 2017/11/17.
 */
@RestController
@CrossOrigin(origins = "*")
public class UserEssayApi {

    @Autowired
    UserEssayService userEssayService;

    @PostMapping("userEssay/save")
    public Result addUserEssay( UserEssayDTO dto){

        if (dto == null) {
            return Result.success("没有提交");
        }
        UserEssay userEssay = new UserEssay();
        BeanUtils.copyProperties(dto,userEssay);
        userEssayService.saveUserEssay(userEssay);
        return Result.success("1");
    }
    @RequestMapping("userEssay/list")
    public PageResult listUserEssay(UserEssayQuery query){

       PageInfo pageInfo =  userEssayService.listUserEssay(query);
       PageResult pageResult = new PageResult();
       BeanUtils.copyProperties(pageInfo,pageResult);
       return  pageResult;
    }
}

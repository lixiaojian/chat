package com.seaworld.utils;

import lombok.Data;

import java.util.List;

/**
 * Description:
 * Created by lixiaojian on 2017/11/17.
 */
@Data
public class PageResult<T> {
    private static final long serialVersionUID = 1L;
    private int pageNum;
    private int pageSize;
    private int size;
    private long total;
    private int pages;
    private List<T> list;
}

package com.seaworld.utils;

import com.sun.net.httpserver.Authenticator;
import lombok.Data;

/**
 * Description:
 * Created by lixiaojian on 2017/11/17.
 */
@Data
public class Result<T> {

    String code;

    String message;

    T data;


    public static <T> Result<T> success(T data){
        Result result = new Result();
        result.setCode("200");
        result.setMessage("success");
        result.setData(data);
        return result;
    }
}

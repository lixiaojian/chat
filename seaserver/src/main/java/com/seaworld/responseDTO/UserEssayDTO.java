package com.seaworld.responseDTO;

import lombok.Data;

import java.util.Date;

/**
 * Description:
 * Created by lixiaojian on 2017/11/17.
 */
@Data
public class UserEssayDTO {

    private Integer userId;

    private String image;

    private String title;

    private String content;


}

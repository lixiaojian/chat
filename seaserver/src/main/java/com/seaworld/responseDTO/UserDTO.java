package com.seaworld.responseDTO;

import lombok.Data;

/**
 * Description:
 * Created by lixiaojian on 2017/11/10.
 */
@Data
public class UserDTO {

    String name;
    Integer age;

}

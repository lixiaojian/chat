package com.seaworld.service;

import com.github.pagehelper.PageInfo;
import com.seaworld.BO.UserEssayQuery;
import com.seaworld.entity.UserEssay;

/**
 * Description:
 * Created by lixiaojian on 2017/11/17.
 */
public interface UserEssayService {

    public int saveUserEssay(UserEssay userEssay);

    public PageInfo listUserEssay(UserEssayQuery query);
}

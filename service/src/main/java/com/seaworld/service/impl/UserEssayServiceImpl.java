package com.seaworld.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.seaworld.BO.UserEssayQuery;
import com.seaworld.entity.UserEssay;
import com.seaworld.entity.UserEssayExample;
import com.seaworld.mapper.UserEssayMapper;
import com.seaworld.service.UserEssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Description:
 * Created by lixiaojian on 2017/11/17.
 */
@Service
public class UserEssayServiceImpl implements UserEssayService {
    @Autowired
    UserEssayMapper userEssayMapper;
    @Override
    public int saveUserEssay(UserEssay userEssay) {
        userEssay.setCreateTime(new Date());
        return userEssayMapper.insert(userEssay);
    }

    @Override
    public PageInfo listUserEssay(UserEssayQuery query) {

        UserEssayExample essayExample = new UserEssayExample();
        UserEssayExample.Criteria criteria = essayExample.createCriteria();
        criteria.andUserIdEqualTo(query.getUserId());
        essayExample.setOrderByClause("create_time desc");
        PageHelper.startPage(query.getCurrentPage(),query.getPageSize());
        List<UserEssay> userEssays = userEssayMapper.selectByExample(essayExample);
        PageInfo pageInfo = new PageInfo(userEssays);
        return pageInfo;


    }
}

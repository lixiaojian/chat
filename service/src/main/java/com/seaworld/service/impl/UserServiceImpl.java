package com.seaworld.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.seaworld.entity.User;
import com.seaworld.entity.UserExample;
import com.seaworld.mapper.UserMapper;
import com.seaworld.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Description:
 * Created by lixiaojian on 2017/11/9.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userDAO;
    @Override
    public PageInfo getUserByPage() {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andAgeEqualTo(18);
        PageHelper.startPage(2, 2);
        List<User> users = userDAO.selectByExample(example);
        PageInfo pageInfo = new PageInfo(users);
        return pageInfo;
    }
}

package com.seaworld.service;

import com.github.pagehelper.PageInfo;

/**
 * Description:
 * Created by lixiaojian on 2017/11/9.
 */
public interface UserService {
    public PageInfo getUserByPage();
}

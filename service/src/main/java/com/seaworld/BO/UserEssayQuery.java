package com.seaworld.BO;

import lombok.Data;

/**
 * Description:
 * Created by lixiaojian on 2017/11/17.
 */
@Data
public class UserEssayQuery {

    Integer userId;

    Integer currentPage;

    Integer pageSize;
}

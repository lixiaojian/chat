package com.seaworld;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ConfigclientApplication {

	public static void main(String[] args) {

		SpringApplication.run(ConfigclientApplication.class, args);
	}
	@Value("${username}")
	String username;
	@Value("${poom}")
	String poom;
	@Value("${password}")
	String password;
	@RequestMapping(value = "/hi")
	public String hi(){
		System.out.println("result:"+username+"--"+poom+"--"+password);
		return username;
	}
}

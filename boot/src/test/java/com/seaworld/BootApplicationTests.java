package com.seaworld;

import com.github.pagehelper.PageInfo;
import com.seaworld.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootApplicationTests {

	@Autowired
	UserService userService;
	@Test
	public void contextLoads() {
		PageInfo pageInfo = userService.getUserByPage();
		System.out.println("result:"+pageInfo.getList().size());
		System.out.println("");
	}

}
